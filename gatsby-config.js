require("dotenv").config()

module.exports = {
  siteMetadata: {
    title: `Curtie Dot Com, Inc.`,
    siteUrl: `http://www.curtie.com/`,
    description: `Curtis Harrison, photographer, Bloomer, Wisconsin.`,
  },
  plugins: [
    "gatsby-plugin-sass",
    "gatsby-plugin-image",
    "gatsby-plugin-sitemap",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: "gatsby-plugin-web-font-loader",
      options: {
        typekit: {
          id: process.env.TYPEKIT_ID,
          families: ['ballinger-condensed']
        },
      },
    },
  ],
};
